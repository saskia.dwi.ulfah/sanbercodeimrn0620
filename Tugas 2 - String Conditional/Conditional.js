//1. If-else

var Nama = "Wina";
var Peran = "Werewolf";

if(Nama == " " && Peran == " ")
{
    console.log("Nama harus diisi!");
}
else if(Nama != " " && Peran == " ")
{
    console.log("Halo " + Nama + ", pilih peranmu untuk memulai game!");

}
else if(Nama != " " && Peran != " ")
{
    console.log("Selamat datang di dunia Werewolf, " +Nama);
    if(Peran == "Penyihir")
    {
        console.log("Halo Penyihir " + Nama + ", kamu dapat melihat siapa yang menjadi werewolf!");
    }
    else if(Peran == "Guard")
    {
        console.log("Halo Guard " + Nama + ", kamu akan membantu melindungi temanmu dari serangan werewolf!");
    }
    else if(Peran == "Werewolf")
    {
        console.log("Halo Werewolf " + Nama + ", kamu akan memakan mangsa setiap malam!");
    }

}

console.log("\n");


//2. Switch-case
var Hari = 18;
var bulan = 10;
var Tahun = 2001;
var Bulan = " "

switch(bulan)
{
    case 1: {Bulan = "Januari"; break;}
    case 2: {Bulan = "Februari"; break;}
    case 3: {Bulan = "Maret"; break;}
    case 4: {Bulan = "April"; break;}
    case 5: {Bulan = "Mei"; break;}
    case 6: {Bulan = "Juni"; break;} 
    case 7: {Bulan = "Juli"; break;}
    case 8: {Bulan = "Agustus"; break;}
    case 9: {Bulan = "September"; break;}
    case 10: {Bulan = "Oktober"; break;}
    case 11: {Bulan = "November"; break;}
    case 12: {Bulan = "Desember"; break;}
}

console.log(Hari+" "+Bulan+" "+Tahun);