//A. Program soal A
console.log("\nJAWABAN SOAL A\n")

function balikString(kata)
{
        var panjang = kata.length;
        var reverseKata = "";
        for(var i = panjang-1;i>=0;i--)
        {
            reverseKata += kata[i];
        }
        return reverseKata;
}

console.log(balikString("abcde")); // edcba
console.log(balikString("rusak")); // kasur
console.log(balikString("racecar")); // racecar
console.log(balikString("haji")); // ijah
console.log("\n");

//B. Program soal B
console.log("\nJAWABAN SOAL B\n")

function palindrome(kata)
{
        var panjang = kata.length;
        var reverseKata = "";
        for(var i = panjang-1;i>=0;i--)
        {
            reverseKata += kata[i];
        }
        
        if (reverseKata == kata){return true;} else {return false;}
}

console.log(palindrome("kasur rusak")); // true
console.log(palindrome("haji ijah")); // true
console.log(palindrome("nabasan")); // false
console.log(palindrome("nababan")); // true
console.log(palindrome("jakarta")); // false

//C. Program soal C
console.log("\nJAWABAN SOAL C\n")

function bandingkan(a, b)
{
    if(a==undefined && b==undefined){return -1;}
    if(b==undefined){b=0;}
    if(a==b){return -1;} else
    if(a<0 || b<0){return -1;} else
    if(a>b){return a;} else
    if(a<b){return b;}
}

console.log(bandingkan(10, 15)); // 15
console.log(bandingkan(12, 12)); // -1
console.log(bandingkan(-1, 10)); // -1 
console.log(bandingkan(112, 121));// 121
console.log(bandingkan(1)); // 1
console.log(bandingkan()); // -1
console.log(bandingkan("15", "18")) // 18


