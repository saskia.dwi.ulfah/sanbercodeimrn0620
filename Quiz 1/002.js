//A. Program soal A
console.log("\nJAWABAN SOAL A\n")

function AscendingTen(a)
{
    if(a==undefined){return -1;} else
    {
    var hasil = String(a).concat(" ");
    var tampung = new Array();
    tampung[0] = a;
    for(var i=1;i<10;i++)
    {
        tampung[i] = tampung[i-1]+ 1;
        hasil += String(tampung[i]).concat(" ");
    }
    return hasil;
    }
}

console.log(AscendingTen(11)) // 11 12 13 14 15 16 17 18 19 20
console.log(AscendingTen(21)) // 21 22 23 24 25 26 27 28 29 30
console.log(AscendingTen())

//B. Program soal B
console.log("\nJAWABAN SOAL B\n");

function DescendingTen(a)
{
    if(a==undefined){return -1;} else
    {
    var hasil = String(a).concat(" ");
    var tampung = new Array();
    tampung[0] = a;
    for(var i=1;i<10;i++)
    {
        tampung[i] = tampung[i-1]-1;
        hasil += String(tampung[i]).concat(" ");
    }
    return hasil;
    }
}

console.log(DescendingTen(100)) 
console.log(DescendingTen(10)) 
console.log(DescendingTen())

//C. Program soal C
console.log("\nJAWABAN SOAL C\n");

function ConditionalAscDesc(reference, check)
{
    if(reference==undefined || check==undefined){return -1;} else
    {
        if(check%2==0)
        {
          var m = DescendingTen(reference);
          return m;
        } else
        if(check%2!=0)
        {
            var n = AscendingTen(reference);
            return n;
        }
    }   
}

console.log(ConditionalAscDesc(20, 8)) 
console.log(ConditionalAscDesc(81, 1)) 
console.log(ConditionalAscDesc(31)) 
console.log(ConditionalAscDesc()) 

//D. Program soal D
console.log("\nJAWABAN SOAL D");

function ularTangga()
{
    var UlarTangga = "";
    for(var i=0;i<10;i++)
    {
        var tampung = new Array();
        if(i%2 == 0)
        {
            for(var j=0;j<10;j++)
            {
                tampung[j] = 100-((i*10)+j);
                UlarTangga += String(tampung[j]).concat(" ");
            }
        }
        else
        if(i%2 != 0)
        {
            for(var k=0;k<10;k++)
            {
                if(i==1){tampung[k]=(100-(i*20))+(k+1);}else{tampung[k]=(100-((i+1)*10))+(k+1)};
                UlarTangga += String(tampung[k]).concat(" ");
            }
        }
        console.log(UlarTangga);
        UlarTangga = "";
    }

}

ularTangga();









