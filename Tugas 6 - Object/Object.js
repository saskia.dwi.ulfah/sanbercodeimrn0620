//1. Program soal 1.
console.log("JAWABAN SOAL NOMOR 1");
console.log("====================\n");

var now = new Date();
var thisYear = now.getFullYear();

function arrToObject (arr){
    if(arr.length==0){console.log("")} else
    {
        for(var i=0;i<arr.length;i++){
            var obj = (i+1) + ". " +arr[i][0].concat(" ", arr[i][1], ": ");
            var x = arr[i][3];

            if(x>thisYear || x==undefined){var usia = "Invalid birth year.";} else {usia = thisYear-x}

            var orang = {
                firstName : arr[i][0],
                lastName  : arr[i][1],
                gender : arr[i][2],
                age : usia
            }
            console.log(obj);
            console.log(orang);
            console.log("\n");
        }
    }   
}

var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ];
var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ];
arrToObject(people);
arrToObject(people2);
arrToObject([]); 

//2. Program soal nomor 2
console.log("\nJAWABAN SOAL NOMOR 2");
console.log("====================\n");

function shoppingTime (memberID, money){
    if(memberID == undefined || memberID == ''|| money == undefined){
        return "Mohon maaf, toko X hanya berlaku untuk member saja.";} else
    if(money < 50000){
        return "Mohon maaf, uang Anda tidak cukup.";
    } else
    {
        var barangBelanja = new Array();
        var kembalian = money;

        for(i=0;i<5;i++){
            if(kembalian>=1500000){
                if(barangBelanja[i-1]=="Sepatu Stacattu"){break;} else{
                barangBelanja.push("Sepatu Stacattu");
                kembalian -= 1500000;}
            } else
            if(kembalian>=500000 && kembalian<1500000) { 
                if(barangBelanja[i-1]=="Baju Zoro"){break;} else{
                barangBelanja.push("Baju Zoro");
                kembalian -= 500000;}
            } else 
            if(kembalian>=250000 && kembalian<500000) {
                if(barangBelanja[i-1]=="Baju H&N"){break;} else{
                barangBelanja.push("Baju H&N"); 
                kembalian -= 250000;}
            } else
            if(kembalian>=175000 && kembalian<250000) {
                if(barangBelanja[i-1]=="Sweater Uniklooh"){break;} else{
                barangBelanja.push("Sweater Uniklooh"); 
                kembalian -= 175000;}
            } else
            if(kembalian>=50000 && kembalian<175000){
                if(barangBelanja[i-1]=="Casing Handphone"){break;} else{
                barangBelanja.push("Casing Handphone"); 
                kembalian -= 50000;}
            }
        } 
        var pembeli = {
            memberId : memberID,
            money : money,
            listPurchased: barangBelanja,
            changeMoney: kembalian
        }
        return pembeli; 
    }   
}


console.log(shoppingTime('1820RzKrnWn08', 2475000));
console.log("\n");
console.log(shoppingTime('82Ku8Ma742', 170000));
console.log("\n");
console.log(shoppingTime('', 2475000)); 
console.log("\n");
console.log(shoppingTime('234JdhweRxa53', 15000));
console.log("\n");
console.log(shoppingTime()); 

//3. Program soal nomor 3
console.log("\nJAWABAN SOAL NOMOR 3");
console.log("====================\n");

function naikAngkot(arrPenumpang){
    if(arrPenumpang.length == 0){return "[]";} else
    {
        var larikObjek = new Array();

         for(i=0;i<2;i++){
            switch(arrPenumpang[i][1]){
                case 'A': {var o1 = 1; break;}
                case 'B': {o1 = 2; break;}
                case 'C': {o1 = 3; break;}
                case 'D': {o1 = 4; break;}
                case 'E': {o1 = 5; break;}
                case 'F': {o1 = 6; break;}
            }

            switch(arrPenumpang[i][2]){
                case 'A': {var o2 = 1; break;}
                case 'B': {o2 = 2; break;}
                case 'C': {o2 = 3; break;}
                case 'D': {o2 = 4; break;}
                case 'E': {o2 = 5; break;}
                case 'F': {o2 = 6; break;}
            }

            var ongkos = (o2-o1)*2000;

            var pnmpg = {
                penumpang: arrPenumpang[i][0],
                naikDari: arrPenumpang[i][1],
                 tujuan: arrPenumpang[i][2],
                bayar: ongkos
            }
            larikObjek[i] = pnmpg;
        }
        return larikObjek;
    }
}
console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
console.log("\n");
console.log(naikAngkot([])); 



