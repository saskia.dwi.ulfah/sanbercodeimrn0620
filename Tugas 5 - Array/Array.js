//1. Program soal nomor 1.
console.log("\nJAWABAN SOAL NOMOR 1");
console.log("====================");

function range(startNum, finishNum)
{
    if(startNum == undefined || finishNum == undefined)
    {
        return -1;
    }
    else
    {
        var larik = new Array();
        if(startNum<finishNum)
        {
            larik[0] = startNum;
            var i = 1;
            while(larik[i-1]<finishNum)
            {
                larik[i] = larik[i-1]+1;
                i++;
            }   
        }
        else if(startNum>finishNum)
        {
            larik[0] = startNum;
            var i = 1;
            while(larik[i-1]>finishNum)
            {
                larik[i] = larik[i-1]-1;
                i++;
            }
        }
        return larik;
    }
}

console.log(range(1, 10));
console.log("\n");
console.log(range(1));
console.log("\n");
console.log(range(11,18)); 
console.log("\n");
console.log(range(54, 50));
console.log("\n");
console.log(range()); 
console.log("\n"); 

//2. Program soal nomor 2.
console.log("JAWABAN SOAL NOMOR 2");
console.log("====================");

function rangeWithStep(startNum, finishNum, step)
{
    var Larik2 = new Array();
    Larik2[0] = startNum;
    if(startNum<finishNum)
    {
        var x = Math.floor((finishNum-startNum)/step+1);
        for(var i=1;i<x;i++)
        {
            Larik2[i] = Larik2[i-1]+step;
        }
    }
    else if(startNum>finishNum)
    {
        var x = Math.floor((startNum-finishNum)/step+1);
        for(var i=1;i<x;i++)
        {
            Larik2[i] = Larik2[i-1]-step;
        }
    }
    return Larik2;
}

console.log(rangeWithStep(1, 10, 2));
console.log("\n");
console.log(rangeWithStep(11, 23, 3)); 
console.log("\n");
console.log(rangeWithStep(5, 2, 1));
console.log("\n");
console.log(rangeWithStep(29, 2, 4)); 
console.log("\n"); 

 //3. Program soal nomor 3.
console.log("JAWABAN SOAL NOMOR 3");
console.log("===================="); 

function sum(a, b, c)
{
    if(a == undefined && b == undefined && c == undefined){return 0;}
    else
    {
    if (b == undefined && c == undefined){b = 0; c = 1;}  else
    if(c == undefined){c = 1;}

    var Larik3 = new Array();
    Larik3[0] = a;
    var jumlah = a;

    if(a<b)
    {
        var x = Math.floor((b-a)/c+1);
        for(var i=1;i<x;i++)
        {
            Larik3[i] = Larik3[i-1]+c;
            jumlah +=  Larik3[i];
        }
    }
    else if(a>b)
    {
        var x = Math.floor((a-b)/c+1);
        for(var i=1;i<x;i++)
        {
            Larik3[i] = Larik3[i-1]-c;
            jumlah += Larik3[i];
        }
    }
    return jumlah; 
    }   
} 

console.log(sum(1, 10)) 
console.log(sum(5, 50, 2)) 
console.log(sum(15, 10))
console.log(sum(20, 10, 2)) 
console.log(sum(1)) 
console.log(sum()) 
console.log("\n"); 

//4. Program soal nomor 4.
console.log("JAWABAN SOAL NOMOR 4");
console.log("===================="); 

function dataHandling(input)
{
    var cetak = "";
    for(var i=0;i<4;i++)
    {
        for(var j=0;j<5;j++)
        {
            if(j==0){cetak += "Nomor ID: ".concat(input[i][j], "\n");} else
            if(j==1){cetak += "Nama Lengkap: ".concat(input[i][j], "\n");} else
            if(j==2){cetak += "TTL: ".concat(input[i][j]," ", input[i][j+1], "\n");} else
            if(j==3){continue};
            if(j==4){cetak += "Hobi: ".concat(input[i][j], "\n");} 
        }
        console.log(cetak);
        cetak = "";
        console.log("\n");
    }
}

var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
] 

dataHandling(input);

//5. Program soal nomor 5.
console.log("JAWABAN SOAL NOMOR 5");
console.log("===================="); 

function balikKata(kalimat)
{
    var panjang = kalimat.length;
    var reverseKata = "";
    for(var i = panjang-1;i>=0;i--)
    {
        reverseKata += kalimat[i];
    }
    return reverseKata;
}

console.log(balikKata("Kasur Rusak"));
console.log(balikKata("SanberCode"));
console.log(balikKata("Haji Ijah")); 
console.log(balikKata("racecar")); 
console.log(balikKata("I am Sanbers")); 
console.log("\n"); 

//6. Program soal nomor 6.
console.log("JAWABAN SOAL NOMOR 6");
console.log("===================="); 

function dataHandling2(arrayData)
{
    //Output 1
    arrayData.splice(4,1, "Pria", "SMA Internasional Metro");
    arrayData[1] = arrayData[1].concat(" " + "Elsharawy");
    arrayData[2] = "Provinsi".concat(" " + arrayData[2]);
    console.log(arrayData);
    console.log("\n");
    
    //Output 2
    var tanggal = arrayData[3].split("/");
    switch(tanggal[1])
    {
        case "01": {console.log("Januari"); break;}
        case "02": {console.log("Februari"); break;}
        case "03": {console.log("Maret"); break;}
        case "04": {console.log("April"); break;}
        case "05": {console.log("Mei"); break;}
        case "06": {console.log("Juni"); break;}
        case "07": {console.log("Juli"); break;}
        case "08": {console.log("Agustus"); break;}
        case "09": {console.log("September"); break;}
        case "10": {console.log("Oktober"); break;}
        case "11": {console.log("November"); break;}
        case "12": {console.log("Desember"); break;}
    }
    console.log("\n");

    //Output 3
    var newTanggal = new Array();
    var newTanggal2 = new Array();
    for(var i=0;i<3;i++)
    {
        newTanggal[i] = Number(tanggal[i]);
    }
    newTanggal.sort();
    for(var i=0;i<3;i++)
    {
        newTanggal2[i] = String(newTanggal[i]);
    }
    newTanggal2[2] = "0".concat(newTanggal2[2]);
    console.log(newTanggal2);
    console.log("\n");

    //Output 4
    var newTanggal3 = tanggal.join("-");
    console.log(newTanggal3);
    console.log("\n");

    //Output 5
    var nama = arrayData[1];
    var name = nama.slice(0,14);
    console.log(name);
    console.log("\n");   
}

var input= ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"];
dataHandling2(input); 




















