// 1. looping While

//LOOPING PERTAMA
console.log("JAWABAN SOAL LOOPING WHILE\n")
console.log("LOOPING PERTAMA");
var number = 2;
while(number<=20){
    console.log(number + " - I love coding");
    number += 2;
}
console.log("\n");

//LOOPING KEDUA
console.log("LOOPING KEDUA");
var angka = 20;
while (angka>=2)
{
    console.log(angka + " - I will become a mobile developer");
    angka -= 2;
} 
console.log("\n");

//2. Looping For

console.log("JAWABAN SOAL LOOPING FOR\n")
var i;
for(i=1; i<=20; i++){
    if(i%2 == 0)
    {
        console.log(i + " - Berkualitas");
    }
    else 
    if(i%2 != 0)
    {
        if(i%3 == 0)
        {
            console.log(i + " - I Love Coding");
        }
        else
        if(i%3 != 0)
        {
            console.log(i + " - Santai");
        }
    }
}
console.log("\n"); 

//3. Membuat persegi panjang #

console.log("JAWABAN SOAL PERSEGI PANJANG\n");
var result = "";
for(var i=0;i<4;i++){
    for(var j=0;j<8;j++){
        result += "#";
    }
    console.log(result);
    result = "";
} 
console.log("\n");

//4. Membuat Tangga

console.log("JAWABAN SOAL MEMBUAT TANGGA\n");
var hasil = "";
for(var i=1;i<=7;i++){
    for(var j=0;j<=i-1;j++){
        hasil += "#";
    }
    console.log(hasil);
    hasil =  "";
} 
console.log("\n");

//5. Membuat catur

console.log("JAWABAN SOAL MEMBUAT CATUR\n");
var catur= "";
var putih = " ";
var hitam = "#";
for(var i=1;i<=8;i++)
{
     for(var j=1;j<=8;j++)
    {
        if(i%2 !=0 && j%2 != 0)
        {
            catur += putih;
        }
        else 
        if(i%2 == 0 && j%2 == 0)
        {
            catur += putih;
        }
        else
        if(i%2 != 0 || j%2 != 0)
        {
            catur += hitam;
        }
    }
    console.log(catur);
    catur = "";
}
console.log("\n");


