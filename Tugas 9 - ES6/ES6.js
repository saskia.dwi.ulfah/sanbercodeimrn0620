//1. Program soal nomor 1
console.log("JAWABAN SOAL NOMOR 1")
console.log("====================\n")

var golden = () => {console.log("This is golden!!")}

golden() 
console.log("\n")

//2. Program soal nomor 2
console.log("JAWABAN SOAL NOMOR 2")
console.log("====================\n")
 
const newFunction = (firstName, lastName)=>{
    return {
        firstName,
        lastName,
        fullName: ()=>{
        console.log(firstName + " " + lastName)
        }
    }   
}
   
newFunction("William", "Imoh").fullName() 
console.log("\n")

//3. Program soal nomor 3
console.log("JAWABAN SOAL NOMOR 3")
console.log("====================\n")

const newObject = {
    firstName: "Harry",
    lastName: "Potter Holt",
    destination: "Hogwarts React Conf",
    occupation: "Deve-wizard Avocado",
    spell: "Vimulus Renderus!!!"
}

const {firstName, lastName, destination, occupation, spell} = newObject
console.log(firstName, lastName, destination, occupation)
console.log("\n")

//4. Program soal nomor 4
console.log("JAWABAN SOAL NOMOR 4")
console.log("====================\n")

const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]

const combined = [...west, ...east]
console.log(combined)
console.log("\n")

//5. Program soal nomor 5
console.log("JAWABAN SOAL NOMOR 5")
console.log("====================\n")

const planet = "earth"
const view = "glass"
var before = `Lorem ${view} dolor sit amet, consectetur adipiscing elit, ${planet} do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam`

console.log(before)
console.log("\n")