//1. Program soal nomor 1

class Score{
    constructor(subject, points, email){
        this.subject = subject
        this.points = points
        this.email = email
    }

    average(){
        if(this.points.length>1){
            let total = 0
            for(let i=0;i<this.points.length;i++){
                total += this.points[i]
            }
            let avrg = total/this.points.length
            return avrg
        }
        else {return this.points}
    }
} 

//2. Program soal nomor 2
var viewScores = (arr, sub) => {
    let x = new Array()
    for (let i=1;i<arr.length;i++){

        if(sub == "quiz-1"){var score = arr[i][1]}
        else
        if(sub == "quiz-2"){var score = arr[i][2]}
        else
        if(sub == "quiz-3"){var score = arr[i][3]}

        var z = {
            email: arr[i][0],
            subject: sub,
            points: score
        }
        x.push(z)
    }
    console.log(x) 
}

const data = [
    ["email", "quiz - 1", "quiz - 2", "quiz - 3"],
    ["abduh@mail.com", 78, 89, 90],
    ["khairun@mail.com", 95, 85, 88],
    ["bondra@mail.com", 70, 75, 78],
    ["regi@mail.com", 91, 89, 93]
]

viewScores(data, "quiz-1")
viewScores(data, "quiz-2")
viewScores(data, "quiz-3")

//3. Program soal nomor 3
var recapScore = (data) => {
    let sum = 0
    for(let i=1; i<data.length;i++){

        sum = sum + data[i][1] + data[i][2]+data[i][3]
        var rerata = (sum/3)

        if(rerata>70 && rerata<= 80){var predikat = "Participant"}
        else
        if(rerata>80 && rerata<= 90){predikat = "Graduate"}
        else
        if(rerata>90){predikat = "Honour"}

        console.log(i + ". Email:  " + data[i][0])
        console.log("Rerata: " + rerata)
        console.log("Predikat: " + predikat + "\n")
        
        sum = 0
    }
}

recapScore(data)



