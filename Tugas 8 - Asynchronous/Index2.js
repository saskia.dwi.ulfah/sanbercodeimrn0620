var readBooksPromise = require('./promise.js')
 
var book = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]
 
// Lanjutkan code untuk menjalankan function readBooksPromise
var waktu = 10000
function runCode(indexOfBook){
    if(indexOfBook>3){return 0} 

    readBooksPromise(waktu, book[indexOfBook])
    .then(function(fulfilled){
        waktu = fulfilled
        runCode(++indexOfBook)
    })
    .catch(function(error){}) 
}

runCode(0)




