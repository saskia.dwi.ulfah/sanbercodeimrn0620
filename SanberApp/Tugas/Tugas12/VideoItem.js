import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View, Image, TouchableOpacity} from 'react-native'
import Icon from 'react-native-vector-icons/MaterialIcons'


export default class VideoItem extends Component{

    render(){
        let video = this.props.video;
        return(
            <View style={styles.container}>
                <Image source={{uri: video.snippet.thumbnails.high.url}} style={{height:200}}/>
                <View style={styles.descContainer}> 
                    <Image source={{uri: 'https://static.wixstatic.com/media/2d23a2_5cf9cb610460400e9098aee6c0017e75.jpg/v1/fill/w_315,h_469,al_c,q_80,usm_0.66_1.00_0.01/2d23a2_5cf9cb610460400e9098aee6c0017e75.jpg'}} style={{width:50, height:50, borderRadius:25}}/>
                    <View style={styles.videoDetails} >
                        <Text numberOfLines={2} style={styles.videoTitle}>{video.snippet.title}</Text>
                        <Text style={styles.videoStats}>{video.snippet.channelTitle + "  .  " + nFormatter(video.statistics.viewCount,1) + "  .  100 years ago"}</Text>
                    </View>
                    <TouchableOpacity>
                        <Icon name="more-vert" size={20} color='#888888'/>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }

}

function nFormatter(num, digits) {
    var si = [
      { value: 1, symbol: "" },
      { value: 1E3, symbol: "k" },
      { value: 1E6, symbol: "M" },
      { value: 1E9, symbol: "G" },
      { value: 1E12, symbol: "T" },
      { value: 1E15, symbol: "P" },
      { value: 1E18, symbol: "E" }
    ];
    var rx = /\.0+$|(\.[0-9]*[1-9])0+$/;
    var i;
    for (i = si.length - 1; i > 0; i--) {
      if (num >= si[i].value) {
        break;
      }
    }
    return (num / si[i].value).toFixed(digits).replace(rx, "$1") + si[i].symbol + 'views';
  }

const styles = StyleSheet.create({
    container:{
        padding: 15
    },
    descContainer: {
        flexDirection: 'row',
        paddingTop: 15
    },
    videoTitle:{
        fontSize: 16,
        color: '#3c3c3c',
    },
    videoDetails:{
      paddingHorizontal: 15,
      flex: 1  
    },
    videoStats:{
        color: 'grey',
        fontSize: 15,
        paddingTop: 3
    }
})