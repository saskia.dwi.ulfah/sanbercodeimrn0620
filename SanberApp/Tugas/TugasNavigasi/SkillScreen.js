import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View, Image, TouchableOpacity, FlatList, ImageBackground, ScrollView} from 'react-native'
import Icon3 from 'react-native-vector-icons/Feather'
import Icon4 from 'react-native-vector-icons/Fontisto'
import Icon5 from 'react-native-vector-icons/MaterialCommunityIcons'


export default class SkillScreen extends Component{
    render(){ 
        return(
            <View style={styles.container}>
                <View style={styles.skillBar}>
                    <Text style={styles.skillLabel}>Homepage</Text>
                </View>
                <ScrollView>
                    <View style={styles.accPhoto}/>
                    <View style={styles.userNameBox}>
                        <Text style={styles.userNameLabel}>saskia.dwi.ulfah</Text>
                    </View>
                    <View style={styles.basicSkill}>
                        <View style={styles.skillsTittlePlace}>
                            <Text style={styles.skillsTittleLabel}>Programming Language</Text>
                        </View>
                        <View style={styles.skillExplanationPlace} height={230} flexDirection={'column'} justifyContent={'center'} paddingLeft={10}>  
                            <Icon5 name='language-c' size={70}></Icon5>
                            <Icon5 name='language-csharp' size={70}></Icon5>
                            <Icon5 name='language-javascript' size={70}></Icon5>
                        </View>
                        <View style={styles.skillsTittlePlace}>
                            <Text style={styles.skillsTittleLabel}>Framework</Text>
                        </View>
                        <View style={styles.skillExplanationPlace} height={115} flexDirection={'column'} justifyContent={'center'} paddingLeft={10}>
                            <Icon5 name='react' size={70}></Icon5>
                        </View>
                        <View style={styles.skillsTittlePlace}>
                            <Text style={styles.skillsTittleLabel}>Technology</Text>
                        </View>
                        <View style={styles.skillExplanationPlace} height={200} flexDirection={'column'} justifyContent={'space-evenly'} paddingLeft={10}>
                            <Icon3 style={styles.gitlabLogo} name='gitlab' size={70} color={'black'}></Icon3>
                            <Icon4 style={styles.gitlabLogo} name='visual-studio' size={70} color={'black'}></Icon4>
                        </View>
                    </View>
                    <TouchableOpacity>
                        <View style={styles.Button}>
                            <Text style={styles.aboutMeTittle}>About Me</Text>
                        </View>
                        <View style={styles.Button}>
                            <Text style={styles.aboutMeTittle}>Log out</Text>
                        </View>
                    </TouchableOpacity>
                
                </ScrollView>
            </View>

        )
    }
}

const styles = StyleSheet.create({
    container:{
        backgroundColor: '#FFFDFD'
    },
    backLogo:{
        marginTop: 40,
        marginLeft: 20,
    },
    skillBar:{
        height: 40,
        width: 293,
        alignSelf: 'flex-end',
        marginTop: 30,
        backgroundColor: '#D57794'
    },
    skillLabel:{
        fontSize: 23,
        color: '#FFFFFF',
        alignSelf: 'flex-start',
        paddingTop: 4,
        paddingLeft: 7
    },
    accPhoto:{
        height: 150,
        width: 150,
        borderRadius: 150,
        alignSelf: 'center',
        backgroundColor: '#000000',
        marginTop: 30
    },
    basicSkill:{
        height: 800,
        width: 340,
        borderRadius: 15,
        backgroundColor: '#C4C4C4',
        alignSelf: 'center',
        marginTop: 15,
    },
    skillsTittlePlace:{
        height: 40,
        width: 270,
        backgroundColor: '#BEEEF4',
        alignSelf: 'center',
        borderRadius: 10,
        marginTop: 25
    },
    skillsTittleLabel:{
        alignSelf: 'center',
        color: '#000000',
        fontSize: 24
    },
    skillExplanationPlace:{
        width: 300,
        alignSelf: "center",
        backgroundColor:'#EFADC5',
        marginTop: 10
    },
    Button:{
        height: 40,
        width: 120,
        backgroundColor: '#D57794',
        marginTop: 10,
        alignSelf: 'center',
        borderRadius: 20
    },
    aboutMeTittle:{
        alignSelf: 'center',
        color: '#FFFFFF',
        fontSize: 23,
        paddingTop: 4
    },
    userNameBox:{
        width: 190,
        height: 40,
        marginTop: 20,
        marginBottom: 10,
        backgroundColor:'#C4C4C4',
        alignSelf:'center',
        borderRadius: 20
    },
    userNameLabel:{
        alignSelf: 'center',
        paddingTop: 4,
        fontSize: 24
    },
    levelSkill:{
        fontSize: 20,
    },
    fullSkill:{
        height: 50,
        width: 100,
        color: 'white'
    }
})
