import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View, Image, TouchableOpacity, FlatList} from 'react-native'
import Icon from 'react-native-vector-icons/MaterialIcons'


export const Home = ({navigation}) => (
    <View style={styles.container}>
    <View style={styles.appLogo}>
        <Icon style={styles.devLogo} name="developer-mode" size={150} color={'#2F6893'}/>
        <Icon style={styles.peopleLogo} name="group" size={150} color={'#2F6893'}/>  
        <Text style={styles.appName1}>ShowDev!</Text>
        <Text style={styles.appName2}>for branding yourself...</Text>
    </View> 
    <View style={styles.appLoginBox}>
        <Text style={styles.usernameLabel}>Username</Text>
        <TouchableOpacity>
            <View style={styles.loginInputBox}/>
        </TouchableOpacity>
        <Text style={styles.passwordLabel}>Password</Text>
        <TouchableOpacity>
            <View style={styles.loginInputBox}/>
        </TouchableOpacity>
    </View>
    <TouchableOpacity>
        <View style={styles.loginButton}>
            <Text style={styles.loginLabel}>Log in</Text>
        </View>
    </TouchableOpacity>
</View>
)

const styles = StyleSheet.create({
    container:{
        backgroundColor: '#FFFDFD'
    },
    devLogo:{
        justifyContent: 'center',
        alignSelf: 'center',
        marginTop: 125,
        opacity: 0.6,
        borderRadius: 30
    },
    peopleLogo:{
        justifyContent: 'center',
        alignSelf: 'center',
        marginTop: -150,
        opacity: 0.1
    },
    appName1:{
        fontSize: 40,
        alignSelf: 'center',  
        color: '#2F6893'
    },
    appName2:{
        fontSize: 11,
        alignSelf: 'center', 
        marginTop: -5,
        marginLeft: 75,
        color: 'black',
        //fontFamily: 'Metrophobic'
    },
    usernameLabel:{
        marginLeft: 75,
        marginTop: 40,
        fontSize: 14,
        //fontFamily: 'Metrophobic',
        color: 'black'
    },
    passwordLabel:{
        marginLeft: 75,
        marginTop: 5,
        fontSize: 14,
        //fontFamily: 'Metrophobic',
        color: 'black'
    },
    loginInputBox:{
        height: 30,
        width: 222,
        backgroundColor: '#2F6893',
        marginLeft: 75,
        opacity: 0.43,
        borderRadius: 10
    },
    loginButton:{
        height: 30,
        width: 90,
        alignSelf: 'center',
        backgroundColor: '#2F6893',
        opacity: 0.73,
        borderRadius: 5,
        marginTop: 20
    },
    loginLabel:{
        color: '#ffFDFD',
        fontSize: 14,
        marginTop: 6,
        alignSelf: "center",
        //fontFamily: 'Metrophobic'
    }
})