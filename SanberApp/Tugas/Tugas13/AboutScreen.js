import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View, Image, TouchableOpacity, FlatList, ImageBackground} from 'react-native'
import Icon from 'react-native-vector-icons/MaterialIcons'
import Icon2 from 'react-native-vector-icons/Ionicons'
import Icon3 from 'react-native-vector-icons/Feather'
import Icon4 from 'react-native-vector-icons/Fontisto'
import Icon5 from 'react-native-vector-icons/MaterialCommunityIcons'

export default class AboutScreen extends Component{
    render(){
        return(
            <View style={styles.container}>
                <TouchableOpacity>
                    <Icon style={styles.backLogo} name="arrow-back" size={25} color={'black'}></Icon>
                </TouchableOpacity>
                <View style={styles.aboutMeBar}>
                    <Text style={styles.aboutMeLabel}>About Me</Text>
                </View>
                <View style={styles.accPhoto}/>
                <View style={styles.fullNameBox}>
                    <Text style={styles.fullNameLabel}>Fullname</Text>
                </View>
                <View style={styles.myNameBox}>
                    <Text style={styles.myNameLabel}>Saskia Dwi Ulfah</Text>
                </View>
                <View style={styles.cityBar}>
                    <Text style={styles.cityLabel}>City</Text>
                </View>
                <View style={styles.pdgBox}>
                    <Text style={styles.pdgLabel}>Padang</Text>
                </View>
                <View style={styles.fullNameBox}>
                    <Text style={styles.fullNameLabel}>Gitlab</Text>
                </View>
                <View style={styles.gitlabBox}>
                    <Text style={styles.myNameLabel}>https://gitlab.com/saskia.dwi.ulfah</Text>
                </View>
                <View style={styles.socmedPart}>
                    <View style={styles.socmedBox}>
                        <Text style={styles.socmedLabel}>Social Media</Text>
                    </View>
                    <Icon5 name='instagram' size={50}></Icon5>
                    <View style={styles.igBox}>
                        <Text style={styles.igName}>@snowaysky</Text>
                    </View>
                    <Icon5 name='whatsapp' size={50}></Icon5>
                    <View style={styles.igBox}>
                        <Text style={styles.igName}>081374145669</Text>
                    </View>
                </View>
  
 
                

                
            </View>   
        )
    }
}

const styles = StyleSheet.create({
    container:{
        backgroundColor: '#FFFDFD',
    },
    backLogo:{
        marginTop: 40,
        marginLeft: 20
    },
    aboutMeBar:{
        height: 40,
        width: 293,
        alignSelf: 'flex-end',
        marginTop: -30,
        backgroundColor: '#D57794'
    },
    aboutMeLabel:{
        fontSize: 23,
        color: '#FFFFFF',
        alignSelf: 'flex-start',
        paddingTop: 4,
        paddingLeft: 7
    },
    accPhoto:{
        height: 150,
        width: 150,
        borderRadius: 150,
        alignSelf: 'center',
        backgroundColor: 'black',
        marginTop: 30
    },
    fullNameBox:{
        width: 140,
        height: 30,
        backgroundColor: '#BEEEF4',
        marginTop: 30
    },
    fullNameLabel:{
        fontSize: 23,
        color: 'black',
        alignSelf: 'flex-start'
    },
    myNameBox:{
        width: 200,
        height:35,
        backgroundColor: '#D57794'
    },
    myNameLabel:{
        color: 'white',
        alignSelf: 'flex-end',
        fontSize: 23
    },
    cityBar:{
        width: 140,
        height: 30,
        backgroundColor: '#BEEEF4',
        alignSelf:'flex-end'
    },
    cityLabel:{
        color: 'black',
        alignSelf: 'flex-end',
        fontSize: 23
    },
    pdgBox:{
        width: 200,
        height:35,
        backgroundColor: '#D57794',
        alignSelf: 'flex-end'
    },
    pdgLabel:{
        color: 'white',
        alignSelf:'flex-start',
        fontSize: 23
    },
    gitlabBox:{
        height: 35,
        width: 400,
        backgroundColor: '#D57794'
    },
    socmedPart:{
        flexDirection:'column',
        alignItems: 'center'
    },
    socmedBox:{
        width: 200,
        height: 35,
        backgroundColor: '#BEEEF4',
        alignSelf: 'center',
        marginTop: 20
    },
    socmedLabel:{
        alignSelf: 'center',
        fontSize: 23
    },
    igBox:{
        width: 160,
        height: 35,
        backgroundColor: '#D57794',
        marginLeft: 7
    },
    igName:{
        color: 'white',
        fontSize: 23,
        alignSelf: 'center'
    }
})